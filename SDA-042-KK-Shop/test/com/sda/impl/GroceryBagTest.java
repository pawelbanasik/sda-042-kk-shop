package com.sda.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GroceryBagTest {

	GroceryBag groceryBag1;

	@Before
	public void setup() {

		groceryBag1 = new GroceryBag();
//		Item item1 = new Item("Maslo", 3);
//		Item item2 = new Item("Mleko", 2);
//		Item item3 = new Item("Platki", 1);
//		Item item4 = new Item("Chleb", 10);
//		groceryBag1.push(item1);
//		groceryBag1.push(item2);
//		groceryBag1.push(item3);
//		groceryBag1.push(item4);
		
		// pozbywam sie powtorzen
		groceryBag1.push(new Item("Maslo", 3));
		groceryBag1.push(new Item("Mleko", 2));
		groceryBag1.push(new Item("Platki", 1));
		groceryBag1.push(new Item("Chleb", 10));
		
	}

	@Test
	public void testPop() {
		groceryBag1.printStructure();

		// usuwam sobie 2 itemy z gory
		groceryBag1.pop();
		groceryBag1.pop();
		assertTrue(groceryBag1.peek().getName().equals("Mleko"));
	}

}
