package com.sda.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {

	
	@Test
	public void testGetName() {
		Customer customer1 = new Customer("Heniek");
		System.out.println(customer1.getName());
		assertTrue(customer1.getName().equals("Heniek"));
		
	}
	
}
