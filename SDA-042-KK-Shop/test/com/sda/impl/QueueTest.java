package com.sda.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class QueueTest {
	
	Queue queue;
	
	@Before
	public void setup(){
		queue = new Queue();
		
		queue.push(new Customer("Bozenka"));
		queue.push(new Customer("Grazynka"));
		queue.push(new Customer("Helenka"));
		queue.push(new Customer("Marysienka"));
	}
	
	@Test
	public void testPop(){
		queue.printStructure();
		
		queue.pop();
		queue.pop();
		assertTrue(queue.peek().getName().equals("Helenka"));
	}
}
