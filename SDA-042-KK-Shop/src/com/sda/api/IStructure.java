package com.sda.api;

public interface IStructure<T> {
	void push(T item);
	// push dodaje

	T pop();
	// *Pop should remove element from structure
	// @return

	T peek();
	// Peek should not remove element from structure
	// @return Item

	void printStructure();
}
