package com.sda.api;

public interface IItem {
	String getName();
	int getQuantity();
}
