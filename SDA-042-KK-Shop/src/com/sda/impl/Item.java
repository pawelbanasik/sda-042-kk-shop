package com.sda.impl;

import com.sda.api.IItem;
import com.sda.api.IStructure;

public class Item implements IItem {
	private String name;
	private int quantity;

	public Item(String name, int quantity) {
		super();
		this.name = name;
		this.quantity = quantity;

	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getQuantity() {
		return quantity;
	}
}
