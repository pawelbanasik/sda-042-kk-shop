package com.sda.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.sda.api.IStructure;

public class Queue implements IStructure<Customer> {

	List<Customer> listOfCustomers = new LinkedList<>();

	@Override
	public void push(Customer item) {
		listOfCustomers.add(item);

	}

	@Override
	public Customer pop() {

		if (listOfCustomers.isEmpty()) {
			return null;
		}
		// get sluzy do pokazania kto stoi w kolejce
		Customer customerToPop = listOfCustomers.get(0);
		// remove usuwa element
		listOfCustomers.remove(0);
		return customerToPop;
	}

	@Override
	public Customer peek() {
		if (listOfCustomers.isEmpty()) {
			return null;
		}
		// get sluzy do pokazania kto stoi w kolejce
		return listOfCustomers.get(0);

	}

	@Override
	public void printStructure() {
		for (int i = 0; i < listOfCustomers.size(); i++) {
			if (i == 0) {
				System.out.print("NEXT: : ");
			}
			System.out.println(listOfCustomers.get(i).getName());
		}

	}
}
