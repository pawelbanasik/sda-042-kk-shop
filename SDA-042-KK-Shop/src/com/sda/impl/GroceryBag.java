package com.sda.impl;

import java.util.ArrayList;
import java.util.List;

import com.sda.api.IStructure;

public class GroceryBag implements IStructure<Item> {

	List<Item> listOfItems = new ArrayList<>();

	@Override
	public void push(Item item) {
		listOfItems.add(item);

	}

	@Override
	public Item pop() {
		if (listOfItems.isEmpty()) {
			return null;
		}

		Item topStackItem = listOfItems.get(listOfItems.size()-1);
		listOfItems.remove(listOfItems.size()-1);
		return topStackItem;
	}

	@Override
	public Item peek() {
		if (listOfItems.isEmpty()) {
			return null;
		}
		return listOfItems.get(listOfItems.size()-1);
	}

	@Override
	public void printStructure() {
		int i = 0;
		for(Item item : listOfItems){
			i++;
			if(i == listOfItems.size()){
				System.out.print("NEXT: ");
			}
			System.out.println(item.getName()+" "+item.getQuantity());
		}
	}

}
