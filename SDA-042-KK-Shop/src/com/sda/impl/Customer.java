package com.sda.impl;

import com.sda.api.ICustomer;

public class Customer implements ICustomer {

	private String name;

	public Customer(String name) {
		super();
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

}
